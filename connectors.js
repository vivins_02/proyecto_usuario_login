const mysql = require('mysql2/promise');

const dotenv = require('dotenv');

dotenv.config();

async function getPoolConnection() {
    const pool = await mysql.createPool({
        host: process.env.DATABASE_HOST,
        database: process.env.DATABASE_NAME,
        user: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASSWORD,
    });

    return pool;
}

module.exports = {
    getPoolConnection
};