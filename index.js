const {
    getPoolConnection
} = require('./connectors.js');

const viewExperiences = 'SELECT * FROM experiencias';

async function responseQuery(query)
{
    const connection = await getPoolConnection()
    const [row] = await connection.query(query)

    return row

}

responseQuery(viewExperiences)
    .then(r =>
        {
        console.log(r)
        process.exit() //Terminar processo
        }
    )
    .catch(error => console.log(error))